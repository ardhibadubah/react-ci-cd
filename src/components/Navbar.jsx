import React from 'react';
import { Link } from 'react-router-dom';

function Navbar() {
  return (
    <div>
      <h1>Menu</h1>
      <ul className='nav justify-content-center'>
        <li className='nav-item'>
          <Link to='/' className='nav-link'>
            Home
          </Link>
        </li>
        <li className='nav-item'>
          <Link to='/counter' className='nav-link'>
            Counter
          </Link>
        </li>
        <li className='nav-item'>
          <Link to='/cart' className='nav-link'>
            Cart
          </Link>
        </li>
        <li className='nav-item'>
          <Link to='/form' className='nav-link'>
            Form
          </Link>
        </li>
      </ul>
      <nav></nav>
    </div>
  );
}

export default Navbar;
