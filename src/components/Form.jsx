import { useRef, useState } from 'react';

const Form = () => {
  const [email, setEmail] = useState();
  const passwordRef = useRef();

  console.log(passwordRef);

  return (
    <>
      <form>
        <input
          type='email'
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          placeholder='Masukkan Email'
        />
        <input
          ref={passwordRef}
          type='password'
          placeholder='Masukkan Password'
          onChange={() => console.log(passwordRef)}
        />
        <button type='submit'>Login</button>
      </form>
    </>
  );
};

export default Form;
