import axios from 'axios';
import { useState, useEffect } from 'react';

function Home() {
  const [data, setData] = useState([]);

  const getDataWithAxios = async () => {
    const res = await axios('https://jsonplaceholder.typicode.com/users');
    setData(res.data);
  };

  useEffect(() => {
    // getData();
    getDataWithAxios();
  }, []);

  return (
    <div>
      <h1>Ini Home</h1>
      <ul>
        {data.map((item) => {
          return <li key={item.id}>{item.name}</li>;
        })}
      </ul>
    </div>
  );
}

export default Home;
