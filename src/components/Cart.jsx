import { useDispatch, useSelector } from 'react-redux';
import { addCart } from '../redux/action/cartAction';

function Cart() {
  const dispatch = useDispatch();
  const cartCount = useSelector((data) => data.cart.cartCount);
  return (
    <div>
      <button onClick={() => dispatch(addCart(cartCount - 1))}>min cart</button>
      <button onClick={() => dispatch(addCart(cartCount + 1))}>add cart</button>
    </div>
  );
}

export default Cart;
