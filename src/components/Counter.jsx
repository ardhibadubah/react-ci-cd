import { useState, useEffect } from 'react';

function Counter() {
  const [count, setCount] = useState(0);
  const [click, setClick] = useState(-1);

  useEffect(() => {
    setClick(click + 1);
    // eslint-disable-next-line
  }, [count]);

  return (
    <>
      <h1>Counter</h1>
      <div>
        <p>Counter = {count}</p>
        <p>Click = {click}</p>
        <button onClick={() => setCount(count + 1)}>plus</button>
        <button onClick={() => setCount(count - 1)}>minus</button>
      </div>
    </>
  );
}

export default Counter;
