import { useSelector, useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { getListItem } from '../redux/action/cartAction';
import Cart from './Cart';

function Redux() {
  const globalStore = useSelector((data) => data.cart);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getListItem());
    // getData();
  }, []);

  return (
    <>
      <h3>Redux</h3>
      <p>Jumlah Cart: {globalStore.cartCount}</p>
      <Cart />
    </>
  );
}

export default Redux;
