import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import App from '../App';

test('renders learn react link', () => {
  render(<App />);
  const helloWorld = screen.getByText('Hello World');
  expect(helloWorld).toBeInTheDocument();
});

it('should be render Hello World', () => {
  const initialState = {};
  const middleware = [thunk];
  const mockStore = configureMockStore(middleware);
  const store = mockStore(initialState);

  render(
    <Provider store={store}>
      <App />
    </Provider>
  );
  const helloWorld = screen.getByText('Hello World');
  expect(helloWorld).toBeInTheDocument();
});
