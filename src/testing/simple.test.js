const perkalian = (angka1, angka2) => {
  return angka1 * angka2;
};

test('penjumlahan', () => {
  expect(1 + 1).toBe(2);
  expect(5 + 3).toBe(8);
});

it('should be true', () => {
  expect(1 !== 5).toBe(true);
  expect(5 > 3).toBe(true);
});

test('Perkalian', () => {
  expect(perkalian(2, 4)).toBe(8);
});
