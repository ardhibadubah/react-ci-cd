import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import Counter from './components/Counter';
import Navbar from './components/Navbar';
import Home from './components/Home';
import Redux from './components/Redux';
import Form from './components/Form';

function App() {
  // const getData = async () => {
  //   const req = await fetch('https://jsonplaceholder.typicode.com/users');
  //   const res = await req.json();
  //   console.log(req);
  //   setData(res);
  // }

  return (
    <>
      <h1>Hello World</h1>
      <Router>
        <Navbar />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/counter' element={<Counter />} />
          <Route path='/cart' element={<Redux />} />
          <Route path='/form' element={<Form />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
