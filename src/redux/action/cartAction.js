import axios from 'axios';

export const addCart = (data) => {
  return {
    type: 'SET_CART_COUNT',
    payload: data,
  };
};

export const getListItem = () => {
  return async (dispatch) => {
    try {
      const result = await axios(`${process.env.REACT_APP_BASE_API_URL}/items`);
      dispatch({ type: 'SET_LIST_ITEM', payload: result.data });
    } catch (e) {
      console.error(e);
    }
  };
};
