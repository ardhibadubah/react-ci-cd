const initialState = {
  cartCount: 0,
  namaProduk: '',
  harga: 0,
  stock: 0,
  listItem: [],
};

// * (initialState dijadikan nilai default dari parameter state)
const cartReducer = (state = initialState, action) => {
  // const {type, payload} = action

  switch (action.type) {
    case 'SET_CART_COUNT': {
      return {
        ...state,
        cartCount: action.payload,
      };
    }
    case 'SET_LIST_ITEM':
      return {
        ...state,
        listItem: action.payload,
      };
    default:
      return {
        ...state,
      };
  }
};

export default cartReducer;
